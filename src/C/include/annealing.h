#ifndef ANNEALING
#define ANNEALING

#include "solution.h"
#include <stdlib.h>
#include <stdbool.h>

extern bool VERBOSE;
extern size_t MAX_ITERATIONS;
extern size_t MAX_ITER_WITHOUT_IMPROVEMENT;
extern size_t MAX_ITER_SAME_TEMP;
extern size_t MAX_ACCEPTED_SOLUTIONS;

extern size_t ITER_WITHOUT_IMPROVEMENT;

void generate_initial_solution(solution_t * solutions);

void generate_solutions(solution_t * solutions);

void cool_system();

solution_t best_solution();

// void print_solutions(solution_t * solutions);

#endif
