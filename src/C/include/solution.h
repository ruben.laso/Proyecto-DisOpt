#ifndef SOLUTION
#define SOLUTION

typedef struct {
    int n_cpus_0;
    int n_cpus_1;
    int n_cpus_2;
    int n_gpus_0;
    int n_gpus_1;
    int n_knls;
    double fitness;
    long int flops;
    long int watts;
    long int price;
} solution_t;

void generate_random_solution(solution_t * sol);

void print_solution(const solution_t sol);

void fitness_function(solution_t * sol);

void mutate_solution(solution_t * sol);

void compute_attributes(solution_t * sol);

#endif
