\section{Métodos numéricos} \label{sec:numericos}
Para el proceso de optimización del diseño de nuestro clúster también se ha utilizado la \textit{toolbox} de optimización de MATLAB, que provee un amplio conjunto de herramientas para la búsqueda de mínimos para un problema dado.

En concreto, aquí se ha utilizado el \textit{solver} \texttt{fmincon}~\cite{fmincon} con el algoritmo \textit{Interior Point}.
Este método busca el mínimo del problema
\begin{equation*}
	\min_x f(x) \text{ tal que }
	\begin{cases}
	c(x) \leq 0
	\\
	ceq(x) = 0
	\\
	A \cdot x \leq b
	\\
	Aeq \cdot x = beq
	\\
	lb \leq x \leq ub
	\end{cases}
\end{equation*}
donde $beq$ y $b$ son vectores y $Aeq$ y $A$ matrices que nos servirán para determinar las igualdades y desigualdades lineales respectivamente, que nos servirán para definir las restricciones de nuestro problema. Por otro lado, $ceq(x)$ y $c(x)$ son funciones que devuelven vectores para imponer una restricción de igualdad o desigualdad no lineal, y $f(x)$ es la función cuyo valor queremos optimizar y que devuelve un escalar. Además, $x$, $lb$, $ub$ pueden ser vectores o matrices, donde $x$ representa una solución, $lb$ la frontera inferior y $ub$ la frontera superior del espacio de soluciones.

En nuestro problema tendremos que:
\begin{itemize}
	\item $x \in \mathbb{R}^6$: la implementación de MATLAB buscará soluciones en el espacio vectorial hexadimensional de números reales. Una vez tengamos una solución, tendremos que manipular estos números reales para convertirlos en enteros.
	
	\item $lb = (0, 0, 0, 0, 0, 0)$, $ub = (\infty, \infty, \infty, \infty, \infty, \infty)$: en nuestro caso sólo tendremos frontera inferior, donde no podemos tener un número negativo de servidores de un tipo. Al mismo tiempo, no tenemos frontera superior (no es necesaria definirla en MATLAB).
	
	\item $c(x)$ y $ceq(x)$: estas funciones quedan indefinidas, pues no son necesarias para resolver el problema.
	
	\item $Aeq$ y $beq$: dado que no buscamos igualdades, estos elementos quedan indefinidos.
	
	\item $A$ y $b$: para establecer las desigualdades lineales. En nuestro caso tendremos que definir la desigualdad
	\begin{equation*}
		f_0 - \sum_{i=1}^{n} x_i f_i \leq 0
	\end{equation*}
	de forma que tendremos los elementos de $A$ y $b$ definidos tal que:
	\begin{align*}
		a_{1i} & = -f_i, & i & = 1,\dots,n,                      \\
		a_{ji} & = 0,    & j & = 2,\dots,n, \quad i = 1,\dots,n, \\
		b_1    & = -f_0, &                                       \\
		b_j    & = 0,    & j & = 2,\dots,n
	\end{align*}
	donde $f_0$ es la cifra objetivo de rendimiento para nuestro clúster y $f_i$ el rendimiento en GFlops del componente $i$. De esta forma buscaremos soluciones que tengan un rendimiento total mayor que el objetivo marcado.
	
	\item $f(x)$: por último, la función a optimizar es prácticamente idéntica a la vista en la Expresión~\eqref{eq:fitness} donde la única diferencia viene dada por la desaparición del factor de penalización asociado al objetivo de rendimiento. Así pues, la función quedaría tal que:
	\begin{equation}\label{eq:f-numerical}
		f(x)
		= 
		\omega_0 \sum_{i=1}^{n} x_i \frac{p_i}{p_{\text{máx}}}
		+
		\omega_1 \sum_{i=1}^{n} x_i \frac{w_i}{w_{\text{máx}}}
		+
		B(\mathbf{x})
	\end{equation}
	donde $B(\mathbf{x})$ es la contribución de penalización mostrada en la Expresión~\eqref{eq:balance}. Este término es necesario aquí para tomar en consideración el equilibrio entre configuraciones de los servidores.
\end{itemize}

\subsection{Algoritmo \textit{Interior Point}}
El algoritmo \textit{Interior Point} es el recomendado por MATLAB como primera alternativa a tener en cuenta para problemas de minimización. Este método utiliza aproximaciones de los problemas originales, de forma que parte de
\begin{gather*}
	\min_x f(x) \text{ tal que }
	\begin{cases}
	g(x) = 0
	\\
	h(x) \leq 0
	\end{cases}
\end{gather*}
y utilizando variables \textit{slack}, $s_i > 0$, y funciones barrera logarítmicas, convierte la desigualdad de $g(x)$ en una igualdad. Así obtenemos el problema
\begin{gather*}
	\min_x f_\mu(x) = \min_x f(x) - \mu\sum_{i} \ln(s_i) \text{ tal que }
	\begin{cases}
	g(x) = 0
	\\
	h(x) + s = 0
	\end{cases}
\end{gather*}
donde $\mu > 0$ es el parámetro de barrera~\cite{byrd1999interior}.

Para resolver el nuevo problema aproximado se utilizan dos tipos de iteraciones:
\begin{enumerate}
	\item Un paso directo (también denominado de Newton) en $(x,s_i)$, resolviendo el problema por aproximación lineal.
	
	\item En caso de no ser posible utilizar Newton, se utiliza el método del gradiente conjugado.
\end{enumerate}
Con esto, el método pretende minimizar la función
\begin{equation*}
	f^*(x,s) = f_\mu (x,s) + \nu \norm{(h(x), g(x) + s)}
\end{equation*}
donde el parámetro $\nu$ se reducirá a medida que se aumenten las iteraciones, de forma que dirija la solución del problema aproximado hacia la solución del problema original~\cite{optimization-alg}.