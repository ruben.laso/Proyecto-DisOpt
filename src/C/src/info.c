// Objectives and restrictions
const int GFLOPS_OBJECTIVE = 1000000;
const float CPUS_BALANCE = 0.6;
const float GPUS_BALANCE = 0.25;
const float KNLS_BALANCE = 0.15;

// numbers are in +-{0...MUTATION_INTERVAL/2}.
// E.g.: MUTATION_INTERVAL = 5 => {-5,-4,...,4,5}
const int MUTATION_INTERVAL = 10;

// Initial random individuals selection
const int CPUS_0_MIN = 100;
const int CPUS_0_MAX = 200;
const int CPUS_1_MIN = 100;
const int CPUS_1_MAX = 250;
const int CPUS_2_MIN = 100;
const int CPUS_2_MAX = 150;

const int GPUS_0_MIN = 50;
const int GPUS_0_MAX = 150;
const int GPUS_1_MIN = 50;
const int GPUS_1_MAX = 150;

const int KNLS_MIN = 50;
const int KNLS_MAX = 100;

/**
 * INFO ABOUT THE COMPONENTS
 */
// Traditional CPUs
const int GFLOPS_CPU_0 = 403; // Intel Haswell E5-4640
const int GFLOPS_CPU_1 = 845; // Intel Haswell E7-8890
const int GFLOPS_CPU_2 = 538; // Intel Haswell E7-4850

const int WATTS_CPU_0 = 105; // Intel Haswell E5-4640
const int WATTS_CPU_1 = 165; // Intel Haswell E7-8890
const int WATTS_CPU_2 = 115; // Intel Haswell E7-4850

const int PRICE_CPU_0 = 2837; // Intel Haswell E5-4640
const int PRICE_CPU_1 = 7174; // Intel Haswell E7-8890
const int PRICE_CPU_2 = 3002; // Intel Haswell E7-4850

// GPGPUs
const int GFLOPS_GPU_0 = 4700; // NVIDIA Tesla P100
const int GFLOPS_GPU_1 = 2910; // NVIDIA Tesla K80

const int WATTS_GPU_0 = 300; // NVIDIA Tesla P100
const int WATTS_GPU_1 = 300; // NVIDIA Tesla K80

const int PRICE_GPU_0 = 6500; // NVIDIA Tesla P100
const int PRICE_GPU_1 = 2155; // NVIDIA Tesla K80

// Intel Xeon Phi KNL
const int GFLOPS_KNL = 3050; // KNL 7250
const int WATTS_KNL = 215; // KNL 7250
const int PRICE_KNL = 2436; // KNL 7250


// Additional data
const int W_MIN = 105; // Minimum watts
const int W_MAX = 300; // Maximum watts

const int P_MIN = 2155; // Minimum price
const int P_MAX = 7174; // Maximum price
