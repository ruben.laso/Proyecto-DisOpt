#ifndef INFO
#define INFO

// Objectives and restrictions
extern const int GFLOPS_OBJECTIVE;
extern const float CPUS_BALANCE;
extern const float GPUS_BALANCE;
extern const float KNLS_BALANCE;

// numbers are in +-{0...MUTATION_INTERVAL/2}.
// E.g.: MUTATION_INTERVAL = 5 => {-5,-4,...,4,5}
extern const int MUTATION_INTERVAL;

// Initial random individuals selection
extern const int CPUS_0_MIN;
extern const int CPUS_0_MAX;
extern const int CPUS_1_MIN;
extern const int CPUS_1_MAX;
extern const int CPUS_2_MIN;
extern const int CPUS_2_MAX;

extern const int GPUS_0_MIN;
extern const int GPUS_0_MAX;
extern const int GPUS_1_MIN;
extern const int GPUS_1_MAX;

extern const int KNLS_MIN;
extern const int KNLS_MAX;

/**
 * INFO ABOUT THE COMPONENTS
 */
// Traditional CPUs
extern const int GFLOPS_CPU_0; // Intel Haswell E5-5280
extern const int GFLOPS_CPU_1; // Intel Haswell E7-8890
extern const int GFLOPS_CPU_2; // Intel Haswell E7-4850

extern const int WATTS_CPU_0; // Intel Haswell E5-5280
extern const int WATTS_CPU_1; // Intel Haswell E7-8890
extern const int WATTS_CPU_2; // Intel Haswell E7-4850

extern const int PRICE_CPU_0; // Intel Haswell E5-5280
extern const int PRICE_CPU_1; // Intel Haswell E7-8890
extern const int PRICE_CPU_2; // Intel Haswell E7-4850

// GPGPUs
extern const int GFLOPS_GPU_0; // NVIDIA Tesla P100
extern const int GFLOPS_GPU_1; // NVIDIA Tesla K80

extern const int WATTS_GPU_0; // NVIDIA Tesla P100
extern const int WATTS_GPU_1; // NVIDIA Tesla K80

extern const int PRICE_GPU_0; // NVIDIA Tesla P100
extern const int PRICE_GPU_1; // NVIDIA Tesla K80

// Intel Xeon Phi KNL
extern const int GFLOPS_KNL; // KNL 7250
extern const int WATTS_KNL; // KNL 7250
extern const int PRICE_KNL; // KNL 7250


// Additional data
extern const int W_MIN; // Minimum watts
extern const int W_MAX; // Maximum watts

extern const int P_MIN; // Minimum price
extern const int P_MAX; // Maximum price

#endif
