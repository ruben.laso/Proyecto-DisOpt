#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <getopt.h>
#include <time.h>
#include <sys/time.h>

#include "solution.h"
#include "genetic.h"

#include "sort.h"

static bool VERBOSE = false;
static bool PLOT_BEST_FITNESS = false;

static const struct option long_opts [] = {
    {"population",      required_argument,  NULL,   'p'},
    {"generations",     required_argument,  NULL,   'g'},
    {"max-no-improve",  required_argument,  NULL,   'm'},
    {"verbose",         no_argument,        NULL,   'v'},
    {"plot-best",       no_argument,        NULL,   'f'},
};

static const char short_opts [] = "p:g:m:vf";

static void process_arguments (int argc, char const * argv []);

int main(int argc, char const *argv[]) {
    solution_t * population;
    solution_t * descendants;
    solution_t best;
    double time_sec;
    struct timeval start, end;

    process_arguments(argc, argv);

    population = (solution_t *) malloc(POPULATION_SIZE * sizeof(solution_t));
    descendants = (solution_t *) malloc(POPULATION_SIZE * sizeof(solution_t));

    // Important to produce different random numbers each time
    srand(time(NULL));

    if (VERBOSE) {
        printf("Generating initial population (%lu)\n", POPULATION_SIZE);
    }

    gettimeofday(&start, NULL);

    generate_initial_solution(population);

    size_t iter_without_improve = 0;
    double best_fitness = best_solution().fitness;

    for (size_t i = 0; i < NUM_GENERATIONS; i++, iter_without_improve++) {
        if (VERBOSE) {
            printf("Generation %lu\n", i);
            printf("\tSelecting parents %lu...\n", i);
        }
        select_parents(population, descendants);

        if (VERBOSE) {
            printf("\tComputing descendants %lu...\n", i);
        }
        compute_descendants(descendants);

        if (VERBOSE) {
            printf("\tMutating population %lu...\n", i);
        }
        mutate_population(descendants);

        if (VERBOSE) {
            printf("\tSelecting best individuals %lu...\n", i);
        }
        select_best_individuals(population, descendants);

        best = best_solution();

        if (VERBOSE) {
            printf("Temporary best solution %lu...\n", i);
            print_solution(best);
        }

        if (PLOT_BEST_FITNESS) {
            printf("%lu;%lf\n", i, best.fitness);
        }

        if (best.fitness < best_fitness) {
            best_fitness = best.fitness;
            iter_without_improve = 0;
        } else if (iter_without_improve >= MAX_ITER_WITHOUT_IMPROVEMENT) {
            printf("Maximum number of iterations without improvement reached at iteration %lu. Exiting...\n", i);
            break;
        }
    }
    gettimeofday(&end, NULL);

    time_sec = (end.tv_sec-start.tv_sec+(end.tv_usec-start.tv_usec)/1.e6);
    printf("Elapsed time: %f\n", time_sec);

    best = best_solution();
    print_solution(best);

    return EXIT_SUCCESS;
}

static void process_arguments (int argc, char const * argv []) {
    int rv;
    for (
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL);
            rv != -1;
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL)
        ) {
        switch (rv) {
            case 'p':
                POPULATION_SIZE = atoi(optarg);
                break;
            case 'g':
                NUM_GENERATIONS = atoi(optarg);
                break;
            case 'm':
                MAX_ITER_WITHOUT_IMPROVEMENT = atoi(optarg);
                break;
            case 'v':
                VERBOSE = true;
                break;
            case 'f':
                PLOT_BEST_FITNESS = true;
                break;
            default:
                break;
        }
    }
}
