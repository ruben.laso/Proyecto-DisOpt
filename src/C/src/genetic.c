#include "genetic.h"
#include "sort.h"

size_t POPULATION_SIZE = 1000;
size_t NUM_GENERATIONS = 10000;
size_t MAX_ITER_WITHOUT_IMPROVEMENT = 1000;

static solution_t best;

void generate_initial_solution(solution_t * population) {
    for (size_t i = 0; i < POPULATION_SIZE; i++) {
        generate_random_solution(&population[i]);
    }

    sort(population, 0, (int) POPULATION_SIZE-1);

    best = population[0];
}

void select_parents(const solution_t * population, solution_t * parents) {
    // just get the POPULATION_SIZE/2 best individuals to be the parents
    for (size_t i = 0; i < POPULATION_SIZE/2; i++) {
        parents[i] = population[i];
    }
}

void compute_descendants(solution_t * descendants) {
    size_t p1;
    size_t p2;

    for (size_t i = POPULATION_SIZE/2; i < POPULATION_SIZE; i+=2) {
        p1 = rand() % (POPULATION_SIZE/2);
        p2 = rand() % (POPULATION_SIZE/2);

        cross_parents(descendants[p1], descendants[p2], &descendants[i], &descendants[i+1]);
    }
}

void inline cross_parents(const solution_t p1, const solution_t p2, solution_t * c1, solution_t * c2) {
    c1->n_cpus_0 = p1.n_cpus_0;
    c1->n_cpus_1 = p1.n_cpus_1;
    c1->n_cpus_2 = p1.n_cpus_2;
    c1->n_gpus_0 = p2.n_gpus_0;
    c1->n_gpus_1 = p2.n_gpus_1;
    c1->n_knls   = p2.n_knls;

    c2->n_cpus_0 = p2.n_cpus_0;
    c2->n_cpus_2 = p2.n_cpus_1;
    c2->n_cpus_1 = p2.n_cpus_2;
    c2->n_gpus_0 = p1.n_gpus_0;
    c2->n_gpus_1 = p1.n_gpus_1;
    c2->n_knls   = p1.n_knls;

    compute_attributes(c1);
    fitness_function(c1);

    compute_attributes(c2);
    fitness_function(c2);
}

void mutate_population(solution_t * population) {
    for (size_t i = 0; i < POPULATION_SIZE; i++) {
        mutate_solution(&population[i]);
    }
}

void select_best_individuals(solution_t * population, solution_t * descendants) {
    size_t i;
    sort(descendants, 0, (int) POPULATION_SIZE-1);
    for (i = 0; i < POPULATION_SIZE; i++) {
        if (population[i].fitness > descendants[0].fitness) {
            break;
        }
    }
    for (size_t j = i; j < POPULATION_SIZE; j++) {
        population[j] = descendants[j-i];
    }
    sort(population, 0, (int) POPULATION_SIZE-1);

    if (best.fitness > population[0].fitness) {
        best = population[0];
    }
}

solution_t best_solution() {
    return best;
}

void print_population(solution_t * population) {
    for (size_t i = 0; i < POPULATION_SIZE; i++) {
        print_solution(population[i]);
    }
}
