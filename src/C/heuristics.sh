#!/usr/bin/env bash

executions=100
programs=(EA SA)

for program in ${programs[@]}; do
    date >> ${program}.results
    for (( i = 0; i < ${executions}; i++ )); do
        printf "\n\tIteration %d\n" ${i} >> ${program}.results
        ./${program}.out >> ${program}.results
        sleep 1
    done
done
