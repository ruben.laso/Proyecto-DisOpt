\section{Resultados} \label{sec:resultados}
Para la obtención de resultados se han utilizado dos \textit{scripts} diferentes, mostrados en los \lstlistingname{s~\ref{lst:script-heuristics} y~\ref{lst:script-numerical}}.

\lstinputlisting[language=BASH, caption={\textit{Script} para la automatización de pruebas de los algoritmos metaheurísticos.}, label={lst:script-heuristics}]{../src/C/heuristics.sh}

Dada la componente aleatoria de los algoritmos metaheurísticos, es positivo poder ejecutar varias veces el programa, para (potencialmente) tener soluciones diferentes. En el mejor de los casos, los resultados serán iguales, por lo que podremos estar bastante seguros de que la solución alcanzada es óptima o está realmente cerca del óptimo global. Sin embargo, los métodos numéricos utilizados en MATLAB no necesitarán de varias ejecuciones, pues al no tener elementos aleatorios obtendremos siempre la misma solución.

\lstinputlisting[language=MATLAB, caption={\textit{Script} para la automatización de pruebas mediante métodos numéricos.}, label={lst:script-numerical}]{../src/MATLAB/optimization.m}

En la \tablename{~\ref{tab:resultados}} se resumen los dos mejores resultados y el peor, obtenidos en la ejecución de los diferentes programas, donde debemos señalar que se han obtenido hasta 67 soluciones ``óptimas'' diferentes (5 de ellas duplicadas).
\input{tablas/resultados}

\subsection{Análisis de los métodos}
En primer lugar analizaremos los algoritmos metaheurísticos. En la \figurename{~\ref{fig:fitness-evolution}} se ve una muestra del comportamiento de los algoritmos de computación evolutiva y temple simulado.
\input{figuras/fitness}
En ella podemos ver que con computación evolutiva encontramos una buena solución en muchas menos iteraciones, puesto que trabajamos con muchos más individuos en cada generación. Además, el valor del \textit{fitness} siempre se mejora, o se mantiene en el peor de los casos, a lo largo de las generaciones. Por otro lado, con temple simulado trabajamos sobre un único individuo, lo que hace que necesitemos muchas más iteraciones para encontrar una solución ``buena''. En la figura se puede apreciar que la aceptación de soluciones peores ocurre con menor frecuencia a medida que aumenta el número de iteraciones realizadas. 

Sobre los resultados obtenidos en las ejecuciones del \textit{script} del \lstlistingname{~\ref{lst:script-heuristics}, se debe comentar la gran diversidad de soluciones arrojadas por los algoritmos heurísticos, donde debemos destacar las 66 soluciones diferentes encontradas por el programa de computación evolutiva, cuya máxima diferencia en la función \textit{fitness} es de apenas un 1\%. En oposición, el de temple simulado ha hallado solamente sobre cinco soluciones distintas (probando así la convergencia del método) que se diferencian entre sí un 0.3\% y que también han sido encontradas por el de computación evolutiva. A continuación se muestran las dos mejores soluciones halladas por estos métodos:
\begin{itemize}
	\item Solución 1:
	\begin{compactitem}
		\item 46 Intel Xeon E5-4640 v4.
		\item 249 Intel Xeon E7-4850 v4.
		\item 131 NVIDIA Tesla P100.
		\item 76 Intel Xeon Phi 7250.
	\end{compactitem}
	
	\item Solución 2:
	\begin{compactitem}
		\item 16 Intel Xeon E5-4640 v4.
		\item 2 Intel Xeon E7-8890 v4.
		\item 274 Intel Xeon E7-4850 v4.
		\item 131 NVIDIA Tesla P100.
		\item 75 Intel Xeon Phi 7250.
	\end{compactitem}
\end{itemize}

Hablando de las ejecuciones utilizadas para obtener los resultados, se ha de comentar que el algoritmo de computación evolutiva se ha detenido al cabo de un máximo de 6,804 generaciones, después de 1,000 generaciones sin mejora. En el mejor de los casos se ha detenido en la iteración 1,108, encontrando muy rápidamente la solución. La distribución en el número de iteraciones en las que ha finalizado este algoritmo se acumula sobre las 2,000 iteraciones y solamente en diez ocasiones se ha detenido más allá de la iteración 4,000. Sobre el tiempo de cálculo tenemos que la ejecución más lenta ha sido de 17.90 segundos, en el caso en que se han computado 6,804 generaciones. Esto implica 380.93 generaciones/segundo ó 379,921 individuos/segundo, realizando este cálculo \textit{a grosso modo}.

En el algoritmo de temple simulado vemos que generalmente se detiene antes de alcanzar el máximo número de iteraciones (10,000,000). En el mejor de los casos se ha detenido en la iteración 1,101,371. En la ejecución más lenta hemos tardado 1.55 segundos en tener el resultado, lo que implica 6,446,181 iteraciones/segundo.

Con estos datos de rendimiento podemos ver que el algoritmo de temple simulado es unas 17 veces más rápido que el de computación evolutiva. En este análisis de rendimiento se deben tener en cuenta muchos factores:
\begin{itemize}
	\item La complejidad computacional de cada iteración es mucho mayor en el algoritmo EC que en el de SA.
	\item En EC se evalúan múltiples soluciones por iteración, mientras que en SA sólo trabajamos con una.
	\item Al trabajar con una única solución, el algoritmo de temple simulado es muy superior en cuanto al provecho de la localidad de los datos dentro de la jerarquía de memoria. Además, se necesita mucha menos memoria para trabajar con este algoritmo.
\end{itemize}

Por último, analizaremos la función \texttt{fmincon} de MATLAB. En primer lugar, se ha de destacar que este algoritmo trabaja con valores continuos y que realmente la salida que nos da es {[}135.6123, 27.7715, 137.8748, 129.4546, 0.2022, 78.3604{]}, aunque evidentemente no podemos montar 0.2022 procesadores. Por ello, se redondea el resultado al siguiente entero.

En la \figurename{~\ref{fig:fmincon}} se muestran varias gráficas que resumen el comportamiento de este método de optimización. En esta figura podemos destacar varios detalles. 
\begin{figure}[htbp]
	\centering
	\includegraphics[width=1\linewidth]{figuras/fmincon}
	\caption{Resumen de la salida gráfica de la función \texttt{fmincon}.}
	\label{fig:fmincon}
\end{figure}
En primer lugar, podemos ver cómo desciende el valor de la función objetivo en tres etapas, con varios saltos importantes. En la primera etapa (hasta la iteración 13) tenemos un descenso muy rápido de la función objetivo, hasta que en las iteraciones 11 y 13 se produce dos saltos muy considerables en la minimización. En este salto se pasa de $f(x_{11}) = 386.96$ a $f(x_{12}) = 367.56$ y de $f(x_{13}) = 364.91$ a $f(x_{14}) = 343.59$. Estos saltos no sólo se producen en el valor de la función objetivo, sino también en el vector de solución, que tiene un cambio en la norma muy notable sobre las soluciones anteriores. A partir de la iteración número 14, se sigue descendiendo de forma constante hasta las iteraciones 23 y 24 donde se vuelve a tomar otro salto. Igualmente, este salto se debe al aumento del \textit{step size} del método numérico. En la última etapa se desciende de forma muy lenta y prácticamente constante hasta alcanzar el resultado final.

Se ha de insistir en la evolución del \textit{step size} a medida que nos acercamos al mínimo. En las primeras iteraciones se toman valores muy altos debido al enorme margen de mejora en la función. Los picos de tamaño en este parámetro coinciden con grandes saltos en el valor de la función objetivo. También se debe destacar el notable descenso del valor del \textit{step size} en las últimas iteraciones, que ``refinan'' el resultado.

Si atendemos al \textit{first-order optimality}, parámetro que se mide como
\begin{equation*}
	\textit{first-order optimality} = \max_i \abs{(\nabla f(x))_i} = \norm{\nabla f(x)}_\infty,
\end{equation*}
tenemos una medida de cuán cerca está un punto $x$ de un óptimo~\cite{fooptimality}, de forma que, cuanto más pequeño sea este valor, más probable es que estemos ante un mínimo de la función (aunque esto no está realmente asegurado).
De la gráfica asociada a este parámetro debemos destacar que los puntos de mayor valor coinciden generalmente con saltos en el coste de la solución. Así tenemos, que cuanto mayor es el \textit{step size} de una iteración, mayor es el valor del \textit{first-order optimality}.

Como último detalle de este método, cabe comentar que en ninguna iteración se han violado las restricciones impuestas y que el algoritmo siempre ha buscado soluciones dentro del espacio permitido.

\subsection{Análisis de soluciones}
En cuanto a las soluciones encontradas por las diferentes técnicas de optimización hemos de destacar tres tipos de soluciones: las que priorizan procesadores Intel Xeon E5-4640 v4, las que priorizan Intel Xeon E7-4850 v4 y las que son más homogéneas.
En las 66 soluciones obtenidas por el algoritmo de computación evolutivo, la diversidad es enorme y podemos encontrar soluciones de los tres tipos, aún teniendo consumos, precios y rendimiento realmente similares (con diferencias del orden del 1\%). El único elemento invariante en las soluciones de los algoritmos heurísticos es que el Intel Xeon E7-8890 v4 queda descartado, seguramente debido a su alto precio. Se debe mencionar que esto no sucede con la solución obtenida con la función \texttt{fmincon} de MATLAB.

También es destacable el hecho de que incluso la peor solución obtenida por los algoritmos heurísticos es mejor que la encontrada por MATLAB, con una diferencia notable, y teniendo en cuenta que hay un mínimo de 66 soluciones mejores que la de MATLAB.

En resumen, tenemos en consideración las siguientes soluciones:
\begin{itemize}
	\item Solución 1:
	\begin{compactitem}
		\item 46 Intel Xeon E5-4640 v4.
		\item 249 Intel Xeon E7-4850 v4.
		\item 131 NVIDIA Tesla P100.
		\item 76 Intel Xeon Phi 7250.
		\item Precio: \$\ 1,914,636.
		\item Consumo: 89,105 \textit{Watts}.
		\item Rendimiento: 1,000,000 GFlops.
	\end{compactitem}
	
	\item Solución 2:
	\begin{compactitem}
		\item 16 Intel Xeon E5-4640 v4.
		\item 2 Intel Xeon E7-8890 v4.
		\item 274 Intel Xeon E7-4850 v4.
		\item 131 NVIDIA Tesla P100.
		\item 75 Intel Xeon Phi 7250.
		\item Precio: \$\ 1,916,488.
		\item Consumo: 88,945 \textit{Watts}.
		\item Rendimiento: 1,000,000 GFlops.
	\end{compactitem}
	
	\item Solución 3:
	\begin{compactitem}
		\item 136 Intel Xeon E5-4640 v4.
		\item 28 Intel Xeon E7-8890 v4.
		\item 138 Intel Xeon E7-4850 v4.
		\item 138 NVIDIA Tesla P100.
		\item 1 NVIDIA Tesla K80.
		\item 79 Intel Xeon Phi 7250.
		\item Precio: \$\ 2,040,579.
		\item Consumo: 91,055 \textit{Watts}.
		\item Rendimiento: 1,007,572 GFlops.
	\end{compactitem}
\end{itemize}

Aquí debemos realizar un ``postprocesado'' de las soluciones, puesto que no tiene ningún sentido instalar sólo 1 ó 2 elementos de un mismo modelo. Por ello, en las soluciones 1 y 2 podríamos eliminar o sustituir los 2 Intel Xeon E7-8890 v4 y la NVIDIA Tesla K80 respectivamente. Aún con esto, las solución~3 parece quedar descartada puesto que conlleva un coste inicial unos \$\ 100,000 más alta, con un consumo unos 2,000 \textit{Watts} mayor que las otras. Las soluciones 1 y 2 son cuantitativamente iguales aunque, como se mencionaba anteriormente, no tiene mucho sentido tener muy pocos procesadores de un solo modelo. Por ello, la solución elegida es:
\begin{compactitem}
	\item 46 Intel Xeon E5-4640 v4.
	\item 249 Intel Xeon E7-4850 v4.
	\item 131 NVIDIA Tesla P100.
	\item 76 Intel Xeon Phi 7250.
	\item Precio: \$\ 1,914,636.
	\item Consumo: 89,105 \textit{Watts}.
	\item Rendimiento: 1,000,000 GFlops.
\end{compactitem}