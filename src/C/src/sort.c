#include "sort.h"

#include <stdio.h>

static size_t partition(solution_t * population, const int first, const int last) {
    solution_t pivot = population[last];
    solution_t aux;
    int i = first - 1;

    for (int j = first; j < last; j++) {
        if (population[j].fitness <= pivot.fitness) {
            i++;
            // swap population[i] and population[j]
            aux = population[i];
            population[i] = population[j];
            population[j] = aux;
        }
    }

    i++;


    // swap population[i] and population[j]
    aux = population[i];
    population[i] = population[last];
    population[last] = aux;

    return i;
}

void sort(solution_t * population, const int first, const int last) {
    if (first < last) {
        int partition_index = partition(population, first, last);

        sort(population, first, partition_index - 1);
        sort(population, partition_index + 1, last);
    }
}
