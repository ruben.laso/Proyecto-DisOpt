#ifndef GENETIC
#define GENETIC

#include "solution.h"
#include "info.h"

#include <stdlib.h>

extern size_t POPULATION_SIZE;
extern size_t NUM_GENERATIONS;
extern size_t MAX_ITER_WITHOUT_IMPROVEMENT;

void generate_initial_solution(solution_t * population);

void select_parents(const solution_t * population, solution_t * parents);

void compute_descendants(solution_t * descendants);

void cross_parents(const solution_t p1, const solution_t p2, solution_t * c1, solution_t * c2);

void mutate_population(solution_t * population);

void select_best_individuals(solution_t * population, solution_t * descendants);

solution_t best_solution();

void print_population(solution_t * population);

#endif
