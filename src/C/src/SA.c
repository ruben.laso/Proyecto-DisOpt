#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <getopt.h>
#include <time.h>
#include <sys/time.h>

#include "solution.h"
#include "annealing.h"

#include "sort.h"

static bool PLOT_FITNESS = false;

static const struct option long_opts [] = {
    {"max-iterations",      required_argument,  NULL,   'm'},
    {"max-no-improve",      required_argument,  NULL,   'n'},
    {"max-iter-cooling",    required_argument,  NULL,   'i'},
    {"max-accept-cooling",  required_argument,  NULL,   'a'},
    {"verbose",             no_argument,        NULL,   'v'},
    {"plot-fitness",        no_argument,        NULL,   'f'},
};

static const char short_opts [] = "m:n:i:a:vf";

static void process_arguments (int argc, char const * argv []);

int main(int argc, char const *argv[]) {
    solution_t solution;
    solution_t best;
    double time_sec;
    struct timeval start, end;

    process_arguments(argc, argv);

    // Important to produce different random numbers each time
    srand(time(NULL));

    if (VERBOSE) {
        printf("Generating initial solution...\n");
    }

    gettimeofday(&start, NULL);

    generate_initial_solution(&solution);

    for (size_t i = 0; i < MAX_ITERATIONS; i++) {
        if (VERBOSE) {
            printf("Iteration %lu\n", i);
            printf("\tGenerating solution %lu...\n", i);
        }
        generate_solutions(&solution);

        if (VERBOSE) {
            printf("\tCooling system %lu...\n", i);
        }
        cool_system();

        if (VERBOSE) {
            printf("Temporary best solution %lu...\n", i);
            best = best_solution();
            print_solution(best);
        }

        if (PLOT_FITNESS) {
            printf("%lu;%lf\n", i, solution.fitness);
        }

        if (ITER_WITHOUT_IMPROVEMENT >= MAX_ITER_WITHOUT_IMPROVEMENT) {
            printf("Maximum number of iterations without improvement reached at iteration %lu. Exiting...\n", i);
            break;
        }
    }
    gettimeofday(&end, NULL);

    time_sec = (end.tv_sec-start.tv_sec+(end.tv_usec-start.tv_usec)/1.e6);
    printf("Elapsed time: %f\n", time_sec);

    best = best_solution();
    print_solution(best);

    return EXIT_SUCCESS;
}

static void process_arguments (int argc, char const * argv []) {
    int rv;
    for (
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL);
            rv != -1;
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL)
        ) {
        switch (rv) {
            case 'm':
                MAX_ITERATIONS = atoi(optarg);
                break;
            case 'n':
                MAX_ITER_WITHOUT_IMPROVEMENT = atoi(optarg);
                break;
            case 'i':
                MAX_ITER_SAME_TEMP = atoi(optarg);
                break;
            case 'a':
                MAX_ACCEPTED_SOLUTIONS = atoi(optarg);
                break;
            case 'v':
                VERBOSE = true;
                break;
            case 'f':
                PLOT_FITNESS = true;
                break;
            default:
                break;
        }
    }
}
