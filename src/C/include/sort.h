#ifndef SORT
#define SORT

#include "solution.h"
#include <stdlib.h>

void sort(solution_t * population, const int first, const int last);

#endif
