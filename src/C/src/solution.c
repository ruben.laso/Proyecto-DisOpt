#include "solution.h"
#include "info.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

static inline void generate_random_individual(solution_t * sol) {
    sol->n_cpus_0 = rand() % CPUS_0_MAX + CPUS_0_MIN;
    sol->n_cpus_1 = rand() % CPUS_1_MAX + CPUS_1_MIN;
    sol->n_cpus_2 = rand() % CPUS_2_MAX + CPUS_2_MIN;

    sol->n_gpus_0 = rand() % GPUS_0_MAX + GPUS_0_MIN;
    sol->n_gpus_1 = rand() % GPUS_1_MAX + GPUS_1_MIN;

    sol->n_knls = rand() % KNLS_MAX + KNLS_MIN;

    compute_attributes(sol);
    fitness_function(sol);
}

void generate_random_solution(solution_t * sol) {
    generate_random_individual(sol);
}

void print_solution(const solution_t sol) {
    printf("Solution:\n");
    printf("\tCPUs = [%u, %u, %u]\n", sol.n_cpus_0, sol.n_cpus_1, sol.n_cpus_2);
    printf("\tGPUs = [%u, %u]\n", sol.n_gpus_0, sol.n_gpus_1);
    printf("\tKNLs = [%u]\n", sol.n_knls);
    printf("\tPerformance = %ld GFLOPS\n", sol.flops);
    printf("\tPower = %ld Watts\n", sol.watts);
    printf("\tPrice = %ld $\n", sol.price);
    printf("\tFitness = %lf\n", sol.fitness);
    printf("\n");
}

static inline double compute_watts_fitness(const solution_t sol) {
    double watts = 0;

    watts += sol.n_cpus_0 * (WATTS_CPU_0 / (double) W_MAX) +
             sol.n_cpus_1 * (WATTS_CPU_1 / (double) W_MAX) +
             sol.n_cpus_2 * (WATTS_CPU_2 / (double) W_MAX);

    watts += sol.n_gpus_0 * (WATTS_GPU_0 / (double) W_MAX) +
             sol.n_gpus_1 * (WATTS_GPU_1 / (double) W_MAX);

    watts += sol.n_knls * (WATTS_KNL / (double) W_MAX);

    return watts;
}

static inline double compute_prices_fitness(const solution_t sol) {
    double price = 0;

    price += sol.n_cpus_0 * (PRICE_CPU_0 / (double) P_MAX) +
             sol.n_cpus_1 * (PRICE_CPU_1 / (double) P_MAX) +
             sol.n_cpus_2 * (PRICE_CPU_2 / (double) P_MAX);

    price += sol.n_gpus_0 * (PRICE_GPU_0 / (double) P_MAX) +
             sol.n_gpus_1 * (PRICE_GPU_1 / (double) P_MAX);

    price += sol.n_knls * (PRICE_KNL / (double) P_MAX);

    return price;
}

static inline double flops_penalty(const solution_t sol) {
    double penalty;

    penalty = sol.n_cpus_0 * GFLOPS_CPU_0 + sol.n_cpus_1 * GFLOPS_CPU_1 + sol.n_cpus_2 * GFLOPS_CPU_2 +
              sol.n_gpus_0 * GFLOPS_GPU_0 + sol.n_gpus_1 * GFLOPS_GPU_1 +
              sol.n_knls * GFLOPS_KNL;

    penalty = fabs( exp(GFLOPS_OBJECTIVE - penalty) - 1.0);

    return penalty;
}

static inline double balance_penalty(const solution_t sol) {
    double penalty;
    double n_total = sol.n_cpus_0 + sol.n_cpus_1 + sol.n_cpus_2 + sol.n_gpus_0 + sol.n_gpus_1 + sol.n_knls;

    penalty = fabs((sol.n_cpus_0 + sol.n_cpus_1 + sol.n_cpus_2) / n_total - CPUS_BALANCE) +
              fabs((sol.n_gpus_0 + sol.n_gpus_1) / n_total - GPUS_BALANCE) +
              fabs((sol.n_knls) / n_total - KNLS_BALANCE);

    penalty = fabs( exp(penalty * 50) - 1.0);

    return penalty;
}

void fitness_function(solution_t * sol) {
    sol->fitness = 0.7 * compute_watts_fitness(*sol) + 0.3 * compute_prices_fitness(*sol) + flops_penalty(*sol) + balance_penalty(*sol);
}

static inline void compute_flops(solution_t * sol) {
    sol->flops = sol->n_cpus_0 * GFLOPS_CPU_0 + sol->n_cpus_1 * GFLOPS_CPU_1 + sol->n_cpus_2 * GFLOPS_CPU_2;
    sol->flops += sol->n_gpus_0 * GFLOPS_GPU_0 + sol->n_gpus_1 * GFLOPS_GPU_1;
    sol->flops += sol->n_knls * GFLOPS_KNL;
}

static inline void compute_watts(solution_t * sol) {
    sol->watts = sol->n_cpus_0 * WATTS_CPU_0 + sol->n_cpus_1 * WATTS_CPU_1 + sol->n_cpus_2 * WATTS_CPU_2;
    sol->watts += sol->n_gpus_0 * WATTS_GPU_0 + sol->n_gpus_1 * WATTS_GPU_1;
    sol->watts += sol->n_knls * WATTS_KNL;
}

static inline void compute_price(solution_t * sol) {
    sol->price = sol->n_cpus_0 * PRICE_CPU_0 + sol->n_cpus_1 * PRICE_CPU_1 + sol->n_cpus_2 * PRICE_CPU_2;
    sol->price += sol->n_gpus_0 * PRICE_GPU_0 + sol->n_gpus_1 * PRICE_GPU_1;
    sol->price += sol->n_knls * PRICE_KNL;
}

void compute_attributes(solution_t * sol) {
    compute_flops(sol);
    compute_watts(sol);
    compute_price(sol);
}

void mutate_solution(solution_t * sol) {
    sol->n_cpus_0 += rand() % MUTATION_INTERVAL - MUTATION_INTERVAL/2;
    sol->n_cpus_0 = sol->n_cpus_0 < 0 ? 0 : sol->n_cpus_0;
    sol->n_cpus_1 += rand() % MUTATION_INTERVAL - MUTATION_INTERVAL/2;
    sol->n_cpus_1 = sol->n_cpus_1 < 0 ? 0 : sol->n_cpus_1;
    sol->n_cpus_2 += rand() % MUTATION_INTERVAL - MUTATION_INTERVAL/2;
    sol->n_cpus_2 = sol->n_cpus_2 < 0 ? 0 : sol->n_cpus_2;

    sol->n_gpus_0 += rand() % MUTATION_INTERVAL - MUTATION_INTERVAL/2;
    sol->n_gpus_0 = sol->n_gpus_0 < 0 ? 0 : sol->n_gpus_0;
    sol->n_gpus_1 += rand() % MUTATION_INTERVAL - MUTATION_INTERVAL/2;
    sol->n_gpus_1 = sol->n_gpus_1 < 0 ? 0 : sol->n_gpus_1;

    sol->n_knls += rand() % MUTATION_INTERVAL - MUTATION_INTERVAL/2;
    sol->n_knls = sol->n_knls < 0 ? 0 : sol->n_knls;

    compute_attributes(sol);
    fitness_function(sol);
}
