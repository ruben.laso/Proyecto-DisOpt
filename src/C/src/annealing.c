#include "annealing.h"
#include "info.h"
#include "sort.h"

#include <stdio.h>
#include <math.h>

// Public variables
bool VERBOSE = false;
size_t MAX_ITERATIONS = 10000000;
size_t ITER_WITHOUT_IMPROVEMENT = 0;
size_t MAX_ITER_WITHOUT_IMPROVEMENT = 100000;

// Constants
size_t MAX_ITER_SAME_TEMP = 100000;
size_t MAX_ACCEPTED_SOLUTIONS = 25000;

// Variables
static double INITIAL_TEMPERATURE = 0.5;
static double TEMPERATURE = 0;

static size_t ITER_SAME_TEMP = 0;
static size_t ACCEPTED_SOLUTIONS = 0;
static size_t NUM_COOLINGS = 0;

// Best solution
static solution_t best;

void generate_initial_solution(solution_t * solution) {
    generate_random_solution(solution);

    TEMPERATURE = INITIAL_TEMPERATURE;

    if (VERBOSE) {
        printf("Initial temperature: %lf\n", TEMPERATURE);
    }

    best = *solution;
}

static inline double probability_function(const solution_t solution, const solution_t candidate) {
    if (VERBOSE) {
        printf("Probability = %lf\n", (1.0 - (candidate.fitness - solution.fitness) / candidate.fitness) * TEMPERATURE);
    }
    return candidate.fitness == INFINITY ? 0 : (1.0 - (candidate.fitness - solution.fitness) / candidate.fitness) * TEMPERATURE;
}

void generate_solutions(solution_t * solution) {
    solution_t candidate = *solution;

    mutate_solution(&candidate);

    if (VERBOSE) {
        printf("Candidate:\n");
        print_solution(candidate);
    }

    if (candidate.fitness < solution->fitness) {
        *solution = candidate;
        if (candidate.fitness < best.fitness) {
            best = candidate;
            ITER_WITHOUT_IMPROVEMENT = 0;
        } else {
            ITER_WITHOUT_IMPROVEMENT++;
        }

        ACCEPTED_SOLUTIONS++;
    } else if ((rand() / (double) RAND_MAX) < probability_function(*solution, candidate)) {
        *solution = candidate;
        ITER_WITHOUT_IMPROVEMENT++;

        if (VERBOSE) {
            printf("-----------------------\n");
            printf("Accepted worst solution\n");
            printf("-----------------------\n");
        }
    }

    ITER_SAME_TEMP++;
}

void cool_system() {
    if (ITER_SAME_TEMP >= MAX_ITER_SAME_TEMP || ACCEPTED_SOLUTIONS >= MAX_ACCEPTED_SOLUTIONS) {
        NUM_COOLINGS++;

        TEMPERATURE = INITIAL_TEMPERATURE / (1 + NUM_COOLINGS);

        ACCEPTED_SOLUTIONS = 0;
        ITER_SAME_TEMP = 0;

        if (VERBOSE) {
            printf("-----------------------------\n");
            printf("----------COOLING------------\n");
            printf("-----------------------------\n");
            printf("New temperature = %lf\n", TEMPERATURE);
        }
    }
}

solution_t best_solution() {
    return best;
}

// void print_solutions(solution_t * solutions);
