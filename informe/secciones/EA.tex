\subsection{Computación Evolutiva}\label{sec:EA}
Los algoritmos de computación evolutiva, o \textit{evolutionary computation} (EC), se basan en la propia evolución de la naturaleza. Para ello se parte de una población inicial de soluciones, que a base de cruzarse y mutar en cada generación darán lugar a sujetos que serán evaluados y donde se seleccionará a los mejores individuos para formar parte de la siguiente generación. En el \algorithmautorefname~\ref{alg:EA} se muestra una visión simplificada de cómo funciona y del pseudo-código que debe ser implementado.

\input{algoritmos/EA}

Para utilizar este algoritmo, primero debemos definir una serie de elementos que caracterizarán a nuestras soluciones, estos serán sus atributos y su función \textit{fitness} (ver Sección~\ref{sec:fitness}).
Para los atributos tendremos seis números que contarán el número de elementos de cada componente, almacenaremos también el valor de la función \textit{fitness} para una solución determinada, así como su consumo, precio y potencia de cálculo. 

El primer paso del algoritmo es generar una población inicial, que se puede realizar de dos formas diferentes. Por un lado, podemos generar individuos de forma totalmente aleatoria. En nuestro caso estos seis valores tendrían que ser enteros positivos, que tomarían cualquier valor. Por otro lado, podemos utilizar nuestro conocimiento previo del problema para ``acercar'' nuestra población a la solución. Por ejemplo, podemos determinar intervalos de valores iniciales, de forma que mantengamos la componente aleatoria al mismo tiempo que refinemos las soluciones iniciales.

Una vez que tenemos nuestra población inicial, se procede con la evolución. En este proceso, se seleccionará a un conjunto, aleatorio o no, de pares de individuos que tomarán el rol de progenitores y serán cruzados dando como resultado dos descendientes por par de progenitores.
A continuación, introducimos una pequeña mutación en los individuos de la población descendiente. Al final de cada iteración seleccionamos los mejores individuos de la población de progenitores y la de descendientes que servirán como entradas para la próxima iteración. Una vez que ha finalizado el algoritmo, seleccionamos al mejor individuo encontrado en el proceso.

Este algoritmo puede tener dos criterios de parada. El criterio más simple es establecer un número máximo de generaciones. Sin embargo, esto puede conllevar demasiado tiempo de computación inútil, por lo que también se define un máximo de iteraciones del algoritmo sin que se haya encontrado mejoras en la solución. Si se alcanza este máximo, el algoritmo se detiene pues asumimos que hemos encontrado una solución óptima o muy cercana al óptimo.

\subsubsection{Implementación} \label{sec:EA-implementacion}
A continuación se darán algunos detalles importantes en la implementación del algoritmo de computación evolutiva. En el \lstlistingname{~\ref{lst:ea}} se muestra el cuerpo principal del algoritmo.
\lstinputlisting[caption={Cuerpo principal del algoritmo evolutivo.}, label={lst:ea}, firstline=54, lastline=94]{../src/C/src/EA.c}
En él se pueden distinguir las principales fases de la evolución de la población inicial generada. Sobre esta población inicial, debemos mencionar que tenemos dos elementos que nos ayudarán a generar individuos: el objetivo de rendimiento y el balance de componentes. Así podemos definir que el número de CPUs estará entre $[100-200]$, $[200-250]$, $[100-150]$ para cada uno de los modelos respectivamente y entre 50 y 150 para las GPUs y entre 50 y 100 para los KNLs. Con ello seguimos manteniendo una población original aleatoria, aunque concentrada en una región ``cercana'' a la solución óptima.

En el proceso de selección de progenitores se ha decidido tomar solamente a la mitad de individuos más prometedores, es decir, aquellos con mejor función \textit{fitness}, ver \lstlistingname{~\ref{lst:ea-parents}}.

\lstinputlisting[caption={Selección de progenitores en el algoritmo evolutivo.}, label={lst:ea-parents}, firstline=20, lastline=25]{../src/C/src/genetic.c}

Para el proceso de cruce, se seleccionan aleatoriamente dos progenitores del conjunto anterior y se combinan sus soluciones. Cada uno de los hijos heredará el número de CPUs de un padre y el número de GPUs y KNLs del otro (ver \lstlistingname{~\ref{lst:ea-cross}).

\lstinputlisting[caption={Cruce de progenitores en el algoritmo evolutivo.}, label={lst:ea-cross}, firstline=39, lastline=59]{../src/C/src/genetic.c}

Para la mutación se establece un balance de $\pm5$ unidades de cada tipo. En caso de que quede algún número negativo, este se corrige a cero, ver \lstlistingname{~\ref{lst:ea-mutation}}.

\lstinputlisting[caption={Mutación de descendientes en el algoritmo evolutivo.}, label={lst:ea-mutation}, firstline=122, lastline=140]{../src/C/src/solution.c}

Por último, como condiciones de finalización tenemos el alcanzar el máximo número de generaciones a computar, o llegar a una determinada cifra de generaciones sin mejora de la solución ``óptima'' encontrada hasta el momento.

Las opciones de ejecución del programa son:
\begin{compactitem}
	\item \texttt{-p}: tamaño de la población.
	\item \texttt{-g}: número máximo de generaciones a computar.
	\item \texttt{-m}: número máximo de generaciones sin mejorar la solución.
	\item \texttt{-v}: activa mensajes durante la ejecución.
	\item \texttt{-f}: imprime en cada iteración el valor de la función \textit{fitness} del mejor individuo.
\end{compactitem}