\subsection{Temple simulado} \label{sec:SA}
El algoritmo de temple simulado o \textit{simulated annealing} (SA) se basa en la termodinámica, en concreto, en la forma en que los metales se enfrían y cristalizan. Este proceso de enfriamiento se realiza gradualmente, en etapas, de forma que cuando la temperatura es alta, las partículas tienen una mayor libertad de movimiento, mientras que a medida que enfriamos el sistema tenderán a organizarse en rígidas estructuras.
En el \algorithmautorefname{~\ref{alg:SA}} se puede ver el modelo computacional inspirado en este proceso físico.

\input{algoritmos/SA}

El primer paso es generar una solución inicial aleatoria, o no, dependiendo del conocimiento que tengamos sobre nuestro problema. Posteriormente se entra en un bucle donde se generará una solución candidata a partir de una mutación de la solución actual. Si la solución candidata es mejor que la actual, se sustituye automáticamente, y si tiene una función \textit{fitness} (ver Sección~\ref{sec:fitness}) más baja que la mejor solución encontrada hasta ese momento se reemplaza la mejor solución. Lo interesante de este algoritmo es ver qué sucede cuando la solución candidata es peor que la actual.

En el temple simulado tenemos un atributo del sistema denominado temperatura, el cual será de vital importancia en el proceso de decisión sobre si una solución se acepta o se descarta. Con esta temperatura $t$ efectuamos la operación
\begin{equation*}
	p(S_c) = e^{(C(S_a) - C(S_c)) / t}
\end{equation*}
que nos devolverá un número comprendido en el intervalo $(0,1]$ que determinará la probabilidad $p(S_c)$ con la que una solución candidata peor será aceptada. A medida que avanza el algoritmo, la temperatura se reducirá por lo que seremos más estrictos con nuestras soluciones. Este sistema permite que, en las primeras fases de cómputo, podamos explorar diferentes zonas del espacio de soluciones por muy malas que parezcan además de que podremos evitar los mínimos locales.

Una vez que se ha aceptado, o no, la solución candidata se comprueba si se cumplen las condiciones de enfriamiento del sistema y, de ser así, se reduce la temperatura. Estas condiciones pueden ser variadas, las más convencionales suelen ser que se haya alcanzado el máximo número de iteraciones con una misma temperatura o que se hayan aceptado un número determinado de soluciones desde el último enfriamiento.

Una de las grandes ventajas del algoritmo de temple simulado es que permite explorar espacios de búsqueda \textit{a priori} poco prometedores, sin comprometer demasiado la convergencia del método~\cite{bertsimas1993simulated}, y por ello sigue siendo uno de los algoritmos más utilizados para optimización.


\subsubsection{Implementación}
A continuación se darán algunos detalles relevantes de la implementación de este algoritmo. En el \lstlistingname{~\ref{lst:sa}} se muestra el cuerpo principal del código, donde se pueden distinguir las partes principales de éste.

\lstinputlisting[caption={Cuerpo principal del algoritmo de temple simulado.}, label={lst:sa}, firstline=47, lastline=73]{../src/C/src/SA.c}

La generación de la solución inicial se hace exactamente igual que en el algoritmo evolutivo, estableciendo una región de valores (ver Sección~\ref{sec:EA-implementacion}). También se realiza de la misma forma la mutación de la solución actual (ver \lstlistingname{~\ref{lst:ea-mutation}}). La diferencia en este caso radica en la función de probabilidad de aceptación $p(S_c)$ de una solución peor, que ha sido cambiada por
\begin{equation}
	p(S_c) = \left(1 - \frac{C(S_c) - C(S_a)}{C(S_c)} \right) t,
\end{equation}
donde $t \in (0, 0.5]$ y se normaliza la distancia entre costes, de forma que cuanto más diferencia haya entre la solución candidata y la actual, más difícil será aceptar la nueva solución (aunque seguirá siendo posible).

Para el enfriamiento del sistema, se propone bajar la temperatura una vez que se han aceptado un determinado número de soluciones o cuando se ha alcanzado un máximo de iteraciones sin realizar el enfriamiento. De esta forma, la nueva temperatura será
\begin{equation}
	t = \frac{t_0}{n + 1},
\end{equation}
donde $n$ representa el número de enfriamientos hasta el momento y $t_0$ es la temperatura inicial del sistema.

También se establece como condiciones de finalización del algoritmo el haber alcanzado un número determinado de iteraciones totales, o un número de iteraciones sin mejora de la solución ``óptima'' hasta ese momento.

Las opciones de ejecución del programa son:
\begin{compactitem}
	\item \texttt{-m}: número máximo de iteraciones a computar.
	\item \texttt{-n}: número máximo de iteraciones sin mejorar la solución.
	\item \texttt{-i}: número máximo de iteraciones sin enfriar el sistema.
	\item \texttt{-a}: número máximo de soluciones aceptadas sin enfriar el sistema.
	\item \texttt{-v}: activa mensajes durante la ejecución.
	\item \texttt{-f}: imprime en cada iteración el valor de la función \textit{fitness} de la solución actual.
\end{compactitem}