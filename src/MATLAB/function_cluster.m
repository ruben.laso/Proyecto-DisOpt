function [ out ] = function_cluster( x )
% function_cluster: function that computes the fitness of a given cluster configuration

cpu1 = x(1);
cpu2 = x(2);
cpu3 = x(3);
gpu1 = x(4);
gpu2 = x(5);
knls = x(6);

% Linear constraints (paste in linear inequalities box)
% A = [-403 -845 -538 -4700 -2910 -3050; 0 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0];
% b = [-1000000; 0; 0; 0; 0; 0];

% CPUS_GFLOPS = [403, 845, 538];
% GPUS_GFLOPS = [4700, 2910];
% KNLS_GFLOPS = [3050];

CPUS_WATTS = [105, 165, 115];
GPUS_WATTS = [300, 300];
KNLS_WATTS = [215];

CPUS_PRICE = [2837, 7174, 3002];
GPUS_PRICE = [6500, 2155];
KNLS_PRICE = [2436];

MAX_WATTS = 300;
MIN_WATTS = 105;
MAX_PRICE = 7174;
MIN_PRICE = 2155;

CPUS_BALANCE = 0.6;
GPUS_BALANCE = 0.25;
KNLS_BALANCE = 0.15;

watts = cpu1 * (CPUS_WATTS(1) / MAX_WATTS) + ...
        cpu2 * (CPUS_WATTS(2) / MAX_WATTS) + ...
        cpu3 * (CPUS_WATTS(3) / MAX_WATTS) + ...
        gpu1 * (GPUS_WATTS(1) / MAX_WATTS) + ...
        gpu2 * (GPUS_WATTS(2) / MAX_WATTS) + ...
        knls * (KNLS_WATTS(1) / MAX_WATTS);

price = cpu1 * (CPUS_PRICE(1) / MAX_PRICE) + ...
        cpu2 * (CPUS_PRICE(2) / MAX_PRICE) + ...
        cpu3 * (CPUS_PRICE(3) / MAX_PRICE) + ...
        gpu1 * (GPUS_PRICE(1) / MAX_PRICE) + ...
        gpu2 * (GPUS_PRICE(2) / MAX_PRICE) + ...
        knls * (KNLS_PRICE(1) / MAX_PRICE);

n0 = cpu1 + cpu2 + cpu3 + gpu1 + gpu2 + knls;

balance_penalty = abs((cpu1+cpu2+cpu3)/n0 - CPUS_BALANCE) + abs((gpu1+gpu2)/n0 - GPUS_BALANCE) + abs((knls)/n0 - KNLS_BALANCE);
balance_penalty = abs( exp(balance_penalty * 50) - 1 );

out = 0.7 * watts + 0.3 * price + balance_penalty;

end  % function_cluster
