function [ out ] = optimization(  )
% optimization: optimization script for the design of a HPC cluster

% Linear constraints
Aineq = [-403 -845 -538 -4700 -2910 -3050; 0 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0];
bineq = [-1000000; 0; 0; 0; 0; 0];

% Initial point
x0 = [150, 150, 150, 100, 100, 100];

% Lower bounds
lb = [0, 0, 0, 0, 0, 0];

% Optimization script
[x,fval,exitflag,output,lambda,grad,hessian] = cluster_opt(x0,Aineq,bineq,lb)

x

fprintf('Final solution:\n');
x = ceil(x)

[watts, price, GFLOPS] = cluster_specifications(x);

watts
price
GFLOPS

end  % optimization
