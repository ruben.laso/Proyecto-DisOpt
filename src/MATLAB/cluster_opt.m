function [x,fval,exitflag,output,lambda,grad,hessian] = cluster_opt(x0,Aineq,bineq,lb)
%% This is an auto generated MATLAB file from Optimization Tool.

%% Start with the default options
options = optimset;
%% Modify options setting
options = optimset(options,'Display', 'iter-detailed');
options = optimset(options,'PlotFcns', {  @optimplotx @optimplotfunccount @optimplotfval @optimplotconstrviolation @optimplotstepsize @optimplotfirstorderopt });
options = optimset(options,'Algorithm', 'interior-point');
options = optimset(options,'Diagnostics', 'on');
[x,fval,exitflag,output,lambda,grad,hessian] = ...
fmincon(@function_cluster,x0,Aineq,bineq,[],[],lb,[],[],options);
