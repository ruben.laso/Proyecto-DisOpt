\section{Definición del proyecto} \label{sec:definicion-proyecto}
En este proyecto se pretende realizar el diseño de un clúster destinado a HPC (\textit{High-Performance Computing}) donde se optimice la potencia de cálculo, el coste económico y el consumo de los componentes que lo conformarán.

El objetivo será realizar una selección de los componentes que se utilizarán para el HPCC (\textit{High-Performance Computing Cluster})~\cite{middleton2011hpcc} donde, para acotar la magnitud del proyecto, ignoraremos elementos como memorias, sistemas de interconexión, sistemas HVAC (\textit{Heating, Ventilating and Air Conditioning}), etc. y nos centraremos en los procesadores y las posibles tarjetas aceleradoras que puedan incorporar estos, como las GPUs enfocadas a GPGPU (\textit{General-Purpose Computing on Graphics Processing Units})~\cite{che2008performance}, o procesadores con la arquitectura \textit{manycore} de Intel como el Intel Xeon Phi~\cite{sodani2016knights}, que cada día están ganando más terreno en el ámbito de la computación de altas prestaciones.

Así, tomaremos un conjunto reducido de posibles servidores: tres modelos de CPUs ``convencionales'', dos de GPUs, y un único modelo de procesadores Intel Xeon Phi, y trataremos de obtener el conjunto de servidores que cumpla unos determinados requisitos.
Estos objetivos serán:
\begin{compactitem}
	\item Alcanzar un rendimiento pico teórico~\cite{dolbeau2018theoretical} de, al menos, 1 PFlops (\textit{Peta Floating Point Operations per Second}).
	
	\item Minimizar el consumo (en \textit{Watts}) del clúster de computación.
	
	\item Minimizar el precio inicial de los componentes.
\end{compactitem}

A mayores, se impondrá una restricción sobre la solución en lo que a la distribución de tipos de servidores se refiere. Así buscaremos que el 60\% de los nodos de computación correspondan a arquitecturas \textit{multicore} convencionales, como serían los procesadores de la gama Intel Xeon E5~\cite{E5-family} o E7~\cite{E7-family}. El 25\% de los nodos buscarían contener GPUs orientadas a GPGPU, como la gama Tesla~\cite{Tesla-family} de NVIDIA. Por último, también se incorporarán un 15\% de servidores con arquitectura \textit{manycore}, como son los procesadores Intel Xeon Phi \textit{Knights Landing} (KNL). Se ha de mencionar que esta restricción será flexible, de forma que servirá más como una directriz que como una restricción \textit{per se}. Así podemos tener que una solución con 63\% de CPUs, 23\% de GPUs y 14\% de KNLs también podrá ser válida.

Esta decisión tiene sentido debido a que se debe buscar un equilibrio entre los tipos de nodos de computación disponibles en el clúster. Para justificar esto, tenemos que, por ejemplo, el rendimiento pico de los servidores con GPUs es muy alto, aunque en el mundo real es verdaderamente difícil llegar a él. Algo similar sucede con los procesadores \textit{manycore}, que a pesar de ser menos complicado llegar a buenas cifras de rendimiento, su uso todavía no está extendido (además de que estos procesadores tienen tiempos de ejecución de los programas secuenciales realmente malos). Por último, es normal que los nodos con procesadores ``convencionales'' sean los más extendidos, pues la mayoría de aplicaciones están optimizadas para este tipo de componentes, y en la práctica son los más utilizados.

Para realizar la optimización del sistema, se implementarán dos algoritmos de búsqueda heurística. Este tipo de técnicas son las más utilizadas cuando se trabaja con valores y parámetros discretos, tal y como es este caso. Así pues, se implementarán los algoritmos de computación evolutiva~\cite{kicinger2005evolutionary} y temple simulado~\cite{brooks1995optimization}, de forma que tendremos uno más centrado en la intensificación y otro en la diversificación de la búsqueda~\cite{hart1995theoretical}.
A mayores, también se pretende utilizar alguno de los métodos numéricos incluidos en las librerías de optimización de MATLAB~\cite{matlab}.

Con esto se pretende estudiar las diferentes técnicas de optimización, sus resultados y rendimiento, además de obtener una configuración ``óptima'' de un clúster destinado a \textit{High Performance Computing}.
